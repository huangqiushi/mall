/**
 * api接口统一管理
 */
import Axios from "axios";      // 引入接口


// 环境的切换,设置公共url
// if (process.env.NODE_ENV === 'development') {
//   Axios.defaults.baseURL = 'http://test.huashenxt.com:3000/'
// } else if (process.env.NODE_ENV === 'production') {
//   Axios.defaults.baseURL = 'https://api.huashenxt.com/'
// }
Axios.defaults.baseURL = 'https://www.luffycity.com/api/v1/';   // 设置公共url


// 添加请求拦截器
Axios.interceptors.request.use(function (config) {
  // 在发送请求之前做些什么
  if (localStorage.getItem('access_token')){
    // 配置的默认值/defaults
    // Axios.defaults.headers.common['Authorization'] = localStorage.getItem('access_token');
    console.log(config.headers);
    config.headers.Authorization = localStorage.getItem('access_token');
  }
  return config;
}, function (error) {
  // 对请求错误做些什么
  return Promise.reject(error);
});


// API
// 获取所有产品
// export const getProductList = () => {   // 匿名函数改为箭头函数
//   return Axios.get('getMallIndex').then(res=>{
//     return res.data;   // 可以简写为.then(res=>res.data);
//   })
// };

// 分类列表
export const categoryList = () => {   // 匿名函数改为箭头函数
  return Axios.get('course_sub/category/list/').then(res=>{
    return res.data;   // 可以简写为.then(res=>res.data);
  })
};

export const allCategoryList = (categoryId) => {
  return Axios.get(`courses/?sub_category=${categoryId}`).then(res=>res.data);
};

// 课程详情顶部
export const courseDetailtop = (courseid)=> {
  return Axios.get('courseDetailTop/?courseId=${courseid}').then(res=res.data);
};
