import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router);

// @绝对路径 检索到 ...src/
export default new Router({
  linkActiveClass: 'is-active',    // linkActiveClass 全局配置 <router-link> 的默认“激活 class 类名”。
  routes: [
    {
      path: '/',
      redirect: '/home'          // 访问/直接跳转/home路径
    },
    {
      path: '/home',
      name: 'Home',
      component: resolve => require(['@/components/Home/Home'], resolve)
    },
    {
      path: '/course',
      name: 'Course',
      component: resolve => require(['@/components/Course/Course'], resolve)
    },
    {
      path: '/course/detail/web/:detailId',   // 动态匹配
      name: 'course.detail',      // 路由名称
      component: resolve => require(['@/components/Course/CourseDetail'], resolve)     // 对应组件
    },
    {
      path: '/free-course',
      name: 'FreeCourse',
      component: resolve => require(['@/components/FreeCourse/FreeCourse'], resolve)
    },
    {
      path: '/micro-course',
      name: 'MicroCourse',
      component: resolve => require(['@/components/MicroCourse/MicroCourse'], resolve)
    }
  ]
})
