// vuex状态管理模式
import Vue from 'vue'
import Vuex from 'vuex'

// 使用插件vuex
Vue.use(Vuex);

const store = new Vuex.Store({   // 容器，包含应用中大部分的状态(state)
  state: {      // 状态
    count: 1
  },
  mutations: {  // 更改状态

  },
  actions: {    // 异步逻辑

  }
});

export default store;    // 导出store对象
